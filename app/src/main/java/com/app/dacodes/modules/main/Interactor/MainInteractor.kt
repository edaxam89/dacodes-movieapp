package com.app.dacodes.modules.main.Interactor

import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.dacodes.R
import com.app.dacodes.common_tools.EndlessRecyclerViewScrollListener
import com.app.dacodes.common_tools.GlobalMembers
import com.app.dacodes.common_tools.NetworkClient
import com.app.dacodes.common_tools.Validate
import com.app.dacodes.modules.main.Adapter.MovieAdapter
import com.app.dacodes.modules.main.Contract.MainContract
import com.app.dacodes.modules.main.Entity.MovieEntity
import com.app.dacodes.modules.main.Service.MainService
import com.app.dacodes.modules.main.View.MainActivity
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainInteractor(var output: MainContract.InteractorOutput): MainContract.Interactor{
    var lastPage = 1
    var totalPages = 1
    var adapterList : MovieAdapter ? = null
    lateinit var scrollListener : EndlessRecyclerViewScrollListener
    override fun onGetMovies(
        activity: MainActivity,
        recyclerView: RecyclerView,
        progressBar: ProgressBar,
        loadpage: Int
    ) {
        progressBar.visibility = View.VISIBLE
        var validate = Validate()
        if(validate!!.verifyAvailableNetwork(activity)) {
            Log.e("loadpage", "" + loadpage)
            val retrofit = NetworkClient().getRetrofitClient()
            val service = retrofit.create(MainService::class.java)
            val call = service.getNowPlaying(GlobalMembers.APIKey, GlobalMembers.language, loadpage)
            call.enqueue(object : Callback<String> {
                override fun onFailure(call: Call<String>, t: Throwable) {
                    progressBar.visibility = View.INVISIBLE
                    output.refreshSuccess()
                    output.showDialog(
                        activity.resources.getString(R.string.sorry), activity.resources.getString(
                            R.string.try_again
                        )
                    )
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    output.refreshSuccess()
                    progressBar.visibility = View.INVISIBLE
                    when (response.code()) {
                        200 -> {
                            val reponseJson = JSONObject(response.body()!!)
                            val results = JSONArray(reponseJson.getString("results"))
                            totalPages = reponseJson.getInt("total_pages")

                            val currentDataList = ArrayList<MovieEntity>()
                            for (item in 0 until results.length()) {
                                var movieObj = results.getJSONObject(item)

                                val id = movieObj.getInt("id")
                                val poster_path = movieObj.getString("poster_path")
                                val title = movieObj.getString("title")
                                val release_date = movieObj.getString("release_date")
                                val vote_average = movieObj.getString("vote_average")

                                val row = MovieEntity(
                                    id,
                                    poster_path,
                                    title,
                                    release_date,
                                    vote_average
                                )
                                currentDataList.add(row)

                            }

                            //If first page
                            if(loadpage == 1){
                                lastPage = 1
                                adapterList = MovieAdapter(currentDataList)
                                recyclerView.adapter = adapterList
                                var linearLayoutManager = GridLayoutManager(
                                    activity,
                                    2,
                                    LinearLayoutManager.VERTICAL,
                                    false
                                )
                                recyclerView?.layoutManager = linearLayoutManager

                                scrollListener =
                                    object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
                                        override fun onLoadMore(
                                            page: Int,
                                            totalItemsCount: Int,
                                            view: RecyclerView
                                        ) {

                                            if(totalPages > lastPage){
                                                lastPage++
                                                onGetMovies(activity,recyclerView,progressBar,lastPage)
                                            }
                                            else{
                                                output.showToast("Hemos llegado al final de la colección")
                                            }

                                        }
                                    }

                                recyclerView.addOnScrollListener(scrollListener)

                                adapterList?.onItemClick = {
                                    output.goToDetail(it)
                                }
                            }
                            else{
                                //only append items
                                scrollListener.resetState()
                                adapterList?.addNewItem(currentDataList)
                                adapterList?.notifyDataSetChanged()
                            }

                        }
                        else -> {
                            output.showDialog(
                                "¡Upps!",
                                "Ocurrio algo inesperado, por favor intente mas tarde, o intenta conectarte a una red más estable"
                            )
                        }
                    }

                }
            })
        }
        else{
            progressBar.visibility = View.INVISIBLE
            output.refreshSuccess()
            output.showDialog(
                activity.resources.getString(R.string.without_network),
                activity.resources.getString(
                    R.string.network_necesary
                )
            )
        }
    }




}