package com.app.dacodes.modules.detail.Service

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface DetailService {
    @Headers("Accept: application/json", "Content-Type: application/json")
    @GET("movie/{id}")
    fun getDetailMovie(@Path("id") id: Int, @Query("api_key") key : String, @Query("language") language : String): Call<String>
}