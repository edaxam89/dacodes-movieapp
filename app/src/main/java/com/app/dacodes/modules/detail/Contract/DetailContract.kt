package com.app.dacodes.modules.detail.Contract

import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.app.dacodes.modules.detail.View.DetailActivity

interface DetailContract {
    interface View{
        fun onShowToast(message: String)
        fun onShowDialog(title : String, message : String)
    }

    interface Interactor{
        fun onGetMovieDetail(activity: DetailActivity, id : Int,imageViewBackdrop: ImageView, textViewTitle: TextView,textViewRunTime: TextView,textViewReleaseDate: TextView,textViewVoteAverange: TextView,textViewGeneres: TextView,textViewdescription: TextView,progressBar : ProgressBar)
    }

    interface InteractorOutput{
        fun showToast(message: String)
        fun showDialog(title : String, message : String)
    }

    interface Presenter{
        fun getMovieDetail(id : Int,imageViewBackdrop: ImageView, textViewTitle: TextView,textViewRunTime: TextView,textViewReleaseDate: TextView,textViewVoteAverange: TextView,textViewGeneres: TextView,textViewdescription: TextView,progressBar : ProgressBar)
    }

}