package com.app.dacodes.modules.main.Entity

data class MovieEntity(val id: Int, val poster_path: String, val title : String, val release_date: String, val vote_average: String)