package com.app.dacodes.modules.main.Presenter

import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.app.dacodes.modules.main.Contract.MainContract
import com.app.dacodes.modules.main.Entity.MovieEntity
import com.app.dacodes.modules.main.Interactor.MainInteractor
import com.app.dacodes.modules.main.Router.MainRouter
import com.app.dacodes.modules.main.View.MainActivity

class MainPresenter (var view : MainContract.View): MainContract.Presenter, MainContract.InteractorOutput{
    var interactor : MainContract.Interactor = MainInteractor(this)
    var router : MainContract.Router = MainRouter(view as MainActivity)
    override fun getMovies(recyclerView: RecyclerView, progressBar: ProgressBar, page: Int) {
        interactor.onGetMovies(view as MainActivity,recyclerView,progressBar, page)
    }

    override fun showToast(message: String) {
        view.onShowToast(message)
    }

    override fun showDialog(title: String, message: String) {
        view.onShowDialog(title, message)
    }

    override fun goToDetail(movieEntity: MovieEntity) {
        router.onGoToDetail(movieEntity)
    }

    override fun refreshSuccess() {
        view.onRefreshSuccess()
    }

}