package com.app.dacodes.modules.main.Contract

import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.app.dacodes.modules.main.Entity.MovieEntity
import com.app.dacodes.modules.main.View.MainActivity

interface MainContract {
    interface View{
        fun onShowToast(message: String)
        fun onShowDialog(title : String, message : String)
        fun onRefreshSuccess()
    }

    interface Presenter{
        fun getMovies(recyclerView: RecyclerView, progressBar: ProgressBar,page : Int)
    }

    interface Interactor{
        fun onGetMovies(activity: MainActivity, recyclerView: RecyclerView,progressBar: ProgressBar,page : Int)
    }

    interface InteractorOutput{
        fun showToast(message: String)
        fun showDialog(title : String, message : String)
        fun goToDetail(movieEntity: MovieEntity)
        fun refreshSuccess()
    }

    interface Router{
        fun onGoToDetail(movieEntity: MovieEntity)
    }
}