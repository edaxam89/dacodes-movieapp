package com.app.dacodes.modules.detail.Interactor

import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.app.dacodes.R
import com.app.dacodes.common_tools.GlobalMembers
import com.app.dacodes.common_tools.NetworkClient
import com.app.dacodes.common_tools.Validate
import com.app.dacodes.modules.detail.Contract.DetailContract
import com.app.dacodes.modules.detail.Service.DetailService
import com.app.dacodes.modules.detail.View.DetailActivity
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailInteractor (var output : DetailContract.InteractorOutput): DetailContract.Interactor{
    override fun onGetMovieDetail(
        activity: DetailActivity,
        id : Int,
        imageViewBackdrop: ImageView,
        textViewTitle: TextView,
        textViewRunTime: TextView,
        textViewReleaseDate: TextView,
        textViewVoteAverange: TextView,
        textViewGeneres: TextView,
        textViewdescription: TextView,
        progressBar : ProgressBar
    ) {
        var validate = Validate()
        progressBar.visibility = View.VISIBLE
        if(validate!!.verifyAvailableNetwork(activity)) {
            val retrofit = NetworkClient().getRetrofitClient()
            val service = retrofit.create(DetailService::class.java)
            val call = service.getDetailMovie(id,GlobalMembers.APIKey,GlobalMembers.language)
            call.enqueue(object : Callback<String> {
                override fun onFailure(call: Call<String>, t: Throwable) {
                    progressBar.visibility = View.INVISIBLE
                    output.showDialog(
                        activity.resources.getString(R.string.sorry), activity.resources.getString(
                            R.string.try_again
                        )
                    )
                }

                override fun onResponse(call: Call<String>, response: Response<String>) {
                    progressBar.visibility = View.INVISIBLE
                    when (response.code()) {
                        200 -> {
                            val reponseJson = JSONObject(response.body()!!)

                            val backdrop_path = GlobalMembers.urlBaseImage + reponseJson.getString("backdrop_path")
                            val title = reponseJson.getString("title")
                            val runtime = reponseJson.getString("runtime")
                            val release_date = reponseJson.getString("release_date")
                            val vote_average = reponseJson.getString("vote_average")
                            val genres = JSONArray(reponseJson.getString("genres"))
                            var genere = ""
                            for (item in 0 until genres.length()) {
                                var genrsObj = genres.getJSONObject(item)
                                genere += " " + genrsObj.getString("name")

                            }
                            val overview = reponseJson.getString("overview")

                            Picasso.get().load(backdrop_path).placeholder(R.drawable.ic_dacodes).into(imageViewBackdrop)
                            textViewTitle.text = title
                            textViewRunTime.text = runtime
                            textViewReleaseDate.text = release_date
                            textViewVoteAverange.text = vote_average
                            textViewGeneres.text = genere
                            textViewdescription.text = overview
                        }
                        else -> {
                            output.showDialog(
                                "¡Upps!",
                                "Ocurrio algo inesperado, por favor intente mas tarde, o intenta conectarte a una red más estable"
                            )
                        }
                    }

                }
            })
        }
        else{
            progressBar.visibility = View.INVISIBLE
            output.showDialog(
                activity.resources.getString(R.string.without_network),
                activity.resources.getString(
                    R.string.network_necesary
                )
            )
        }
    }

}