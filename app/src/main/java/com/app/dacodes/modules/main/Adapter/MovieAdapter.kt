package com.app.dacodes.modules.main.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.dacodes.R
import com.app.dacodes.common_tools.GlobalMembers
import com.app.dacodes.modules.main.Entity.MovieEntity
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat

class MovieAdapter (var listData: ArrayList<MovieEntity>): RecyclerView.Adapter<MovieAdapter.ViewHolder>(){
    private lateinit var view : View

    var onItemClick: ((MovieEntity) -> Unit)? = null
    var listEntity: List<MovieEntity> = listData
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieAdapter.ViewHolder {
        view = LayoutInflater.from(parent.context).inflate(
                R.layout.adapter_item_movie,
                parent,
                false
        )
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieAdapter.ViewHolder, position: Int) {
        Picasso.get().load( GlobalMembers.urlBaseImage + listData[position].poster_path).placeholder(R.drawable.ic_background_poster).into(holder.imageViewPoster)
        holder.textViewTitle?.text = listData[position].title
        val firstDate = listData[position].release_date
        val formatter = SimpleDateFormat("yyyy-MM-dd")
        val date = formatter.parse(firstDate)
        val desiredFormat = SimpleDateFormat("dd-MMM.-yyyy").format(date)

        holder.textViewDate?.text = desiredFormat
        holder.textViewVote?.text = listData[position].vote_average
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        val imageViewPoster = itemView.findViewById<ImageView>(R.id.imageViewPoster)
        val textViewTitle = itemView.findViewById<TextView>(R.id.textViewTitle)
        val textViewDate = itemView.findViewById<TextView>(R.id.textViewDate)
        val textViewVote = itemView.findViewById<TextView>(R.id.textViewVote)
        init {
            itemView.setOnClickListener{
                onItemClick?.invoke(listEntity[adapterPosition])
            }
        }
    }

    fun addNewItem(itemsNew: ArrayList<MovieEntity>){
        //listData.clear() // ->> optional if you need have clear of object
        listData.addAll(itemsNew)

    }
}