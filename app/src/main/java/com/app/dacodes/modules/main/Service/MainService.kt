package com.app.dacodes.modules.main.Service

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface MainService {
    @Headers("Accept: application/json", "Content-Type: application/json")
    @GET("movie/now_playing")
    fun getNowPlaying(@Query("api_key") key : String,@Query("language") language : String,@Query("page") page : Int): Call<String>
}