package com.app.dacodes.modules.detail.Presenter

import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.app.dacodes.modules.detail.Contract.DetailContract
import com.app.dacodes.modules.detail.Interactor.DetailInteractor
import com.app.dacodes.modules.detail.View.DetailActivity

class DetailPresenter (var view : DetailContract.View): DetailContract.Presenter, DetailContract.InteractorOutput{
    var interactor : DetailContract.Interactor = DetailInteractor(this)
    override fun showToast(message: String) {
        view.onShowToast(message)
    }

    override fun showDialog(title: String, message: String) {
        view.onShowDialog(title, message)
    }

    override fun getMovieDetail(
        id: Int,
        imageViewBackdrop: ImageView,
        textViewTitle: TextView,
        textViewRunTime: TextView,
        textViewReleaseDate: TextView,
        textViewVoteAverange: TextView,
        textViewGeneres: TextView,
        textViewdescription: TextView,
        progressBar: ProgressBar
    ) {
        interactor.onGetMovieDetail(view as DetailActivity,id, imageViewBackdrop, textViewTitle, textViewRunTime, textViewReleaseDate, textViewVoteAverange, textViewGeneres, textViewdescription, progressBar)
    }

}