package com.app.dacodes.modules.main.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.app.dacodes.R
import com.app.dacodes.modules.main.Contract.MainContract
import com.app.dacodes.modules.main.Presenter.MainPresenter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),MainContract.View {

    var presenter : MainContract.Presenter? = null
    var currentPage : Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = MainPresenter(this)
        presenter?.getMovies(recyclerView,progressBar, currentPage)

        swipeRefreshLayout.setOnRefreshListener {
            presenter?.getMovies(recyclerView,progressBar, 1)
        }

    }

    override fun onShowToast(message: String) {
        Toast.makeText(this,message,Toast.LENGTH_LONG).show()
    }

    override fun onShowDialog(title: String, message: String) {
        val builder = AlertDialog.Builder(this)

        with(builder) {
            setTitle(title)
            setMessage(message)
            show()
        }
    }

    override fun onRefreshSuccess() {
        swipeRefreshLayout.isRefreshing = false
    }
}