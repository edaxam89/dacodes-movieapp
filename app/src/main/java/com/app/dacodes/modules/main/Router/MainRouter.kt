package com.app.dacodes.modules.main.Router

import android.content.Intent
import com.app.dacodes.modules.detail.View.DetailActivity
import com.app.dacodes.modules.main.Contract.MainContract
import com.app.dacodes.modules.main.Entity.MovieEntity
import com.app.dacodes.modules.main.View.MainActivity

class MainRouter (var activity: MainActivity): MainContract.Router{
    override fun onGoToDetail(movieEntity: MovieEntity) {
        val intent = Intent(activity,DetailActivity::class.java)
        intent.putExtra("id",movieEntity.id)
        intent.putExtra("title",movieEntity.title)
        intent.putExtra("vote_average",movieEntity.vote_average)
        intent.putExtra("release_date",movieEntity.release_date)
        activity.startActivity(intent)
    }

}