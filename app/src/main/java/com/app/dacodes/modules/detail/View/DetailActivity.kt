package com.app.dacodes.modules.detail.View

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.app.dacodes.R
import com.app.dacodes.modules.detail.Contract.DetailContract
import com.app.dacodes.modules.detail.Presenter.DetailPresenter
import kotlinx.android.synthetic.main.activity_deatil.*


class DetailActivity : AppCompatActivity() , DetailContract.View{
    var presenter : DetailContract.Presenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_deatil)
        setSupportActionBar(findViewById(R.id.toolbar))
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()!!.setDisplayShowHomeEnabled(true);

        presenter = DetailPresenter(this)

        val extras = intent.extras
        val id = extras!!.getInt("id")
        val title = extras!!.getString("title")
        val vote_average = extras!!.getString("vote_average")
        val release_date = extras!!.getString("release_date")


        textViewTitle.text = title
        textViewReleaseDate.text = release_date
        textViewVoteAverange.text = vote_average



        presenter?.getMovieDetail(id,imageViewBackdrop,textViewTitle,textViewRunTime,textViewReleaseDate,textViewVoteAverange,textViewGeneres,textViewdescription,progressBar)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()

    }

    override fun onShowToast(message: String) {
        Toast.makeText(this,message, Toast.LENGTH_LONG).show()
    }

    override fun onShowDialog(title: String, message: String) {
        val builder = AlertDialog.Builder(this)

        with(builder) {
            setTitle(title)
            setMessage(message)
            show()
        }
    }

}