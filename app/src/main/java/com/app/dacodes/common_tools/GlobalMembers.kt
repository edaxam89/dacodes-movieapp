package com.app.dacodes.common_tools

class GlobalMembers {
    companion object{
        val urlBaseAPI = "https://api.themoviedb.org/3/"
        val APIKey = "a28c4bc831b590dc669ef8a459fdbff7"
        val language = "es-MX"
        val urlBaseImage = "https://image.tmdb.org/t/p/w500"
    }
}